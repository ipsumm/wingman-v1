﻿// Copyright (c) Microsoft. All rights reserved.

using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using Windows.Devices.Bluetooth;
using Windows.Devices.Bluetooth.Rfcomm;
using Windows.Networking.Sockets;
using Windows.Storage;
using Windows.Storage.Streams;
using Windows.UI.Core;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Media;

namespace IoTCoreDefaultApp
{
    public sealed partial class Wingman : Page, INotifyPropertyChanged
    {
        #region Public Fields

        public const string FEATURE_NAME = "BLUETOOTH PIGEON THROWER";
        public static Wingman Current;

        #endregion Public Fields

        #region Private Fields

        private bool[] _enableJogButtons = new bool[6];
        private ObservableCollection<string> _presetItems = new ObservableCollection<string>();
        private PigeonThrowerPosition[] presets = new PigeonThrowerPosition[26];
        private RfcommServiceProvider rfcommProvider;
        private StreamSocket socket;
        private StreamSocketListener socketListener;
        private StartupTask start = new StartupTask();
        private bool remoteDisconnection = false;
        private DispatcherTimer dispatcherTimer;
        private DataWriter writer;
        private DataReader reader;
        private int commandCount;

        #endregion Private Fields

        #region Public Constructors

        public Wingman()
        {
            this.InitializeComponent();
            Current = this;

            //Initialize the controller
            start.InitAll();

            AddToAppData();
            LoadPresetsToView();
            this.DataContext = this;
            EnableAllJogButtons();
        }

        #endregion Public Constructors

        #region Public Events

        public event PropertyChangedEventHandler PropertyChanged;

        #endregion Public Events

        #region Public Enums

        public enum NotifyType
        {
            StatusMessage,
            ErrorMessage
        };

        #endregion Public Enums

        #region Public Properties

        public bool[] EnableJogButtons
        {
            get { return _enableJogButtons; }
            set { _enableJogButtons = value; }
        }

        public ObservableCollection<string> PresetItems
        {
            get { return this._presetItems; }
        }

        #endregion Public Properties

        #region Public Methods

        public async void InitializeRfcommServer()
        {
            try
            {
                rfcommProvider = await RfcommServiceProvider.CreateAsync(RfcommServiceId.FromUuid(BluetoothConstants.RfcommChatServiceUuid));
            }
            // Catch exception HRESULT_FROM_WIN32(ERROR_DEVICE_NOT_AVAILABLE).
            catch (Exception ex) when ((uint)ex.HResult == 0x800710DF)
            {
                // The Bluetooth radio may be off.
                NotifyUser("Make sure your bluetooth radio is on: " + ex.Message, NotifyType.ErrorMessage);
                return;
            }

            // Create a listener for this service and start listening
            socketListener = new StreamSocketListener();
            socketListener.ConnectionReceived += OnConnectionReceived;
            var rfcomm = rfcommProvider.ServiceId.AsString();

            //NotifyUser("SOCKETLISTNER: " + socketListener.ToString() + "\n", NotifyType.StatusMessage);

            try
            {
                await socketListener.BindServiceNameAsync(rfcomm, SocketProtectionLevel.BluetoothEncryptionAllowNullAuthentication);

                // Set the SDP attributes and start Bluetooth advertising
                InitializeServiceSdpAttributes(rfcommProvider);

                rfcommProvider.StartAdvertising(socketListener, true);
            }
            catch (Exception e)
            {
                // If you aren't able to get a reference to an RfcommServiceProvider, tell the user why.  Usually throws an exception if user changed their privacy settings to prevent Sync w/ Devices.
                NotifyUser(e.Message, NotifyType.ErrorMessage);
                return;
            }

            //NotifyUser("LISTENING FOR INCOMING CONNECTIONS", NotifyType.StatusMessage);
            Debug.WriteLine("LISTENING FOR INCOMING CONNECTIONS");
        }

        public void InitializeServiceSdpAttributes(RfcommServiceProvider rfcommProvider)
        {
            var sdpWriter = new DataWriter();

            // Write the Service Name Attribute.
            sdpWriter.WriteByte(BluetoothConstants.SdpServiceNameAttributeType);

            // The length of the UTF-8 encoded Service Name SDP Attribute.
            sdpWriter.WriteByte((byte)BluetoothConstants.SdpServiceName.Length);

            // The UTF-8 encoded Service Name value.
            sdpWriter.UnicodeEncoding = UnicodeEncoding.Utf8;
            sdpWriter.WriteString(BluetoothConstants.SdpServiceName);

            // Set the SDP Attribute on the RFCOMM Service Provider.
            rfcommProvider.SdpRawAttributes.Add(BluetoothConstants.SdpServiceNameAttributeId, sdpWriter.DetachBuffer());
        }

        public void NotifyUser(string strMessage, NotifyType type)
        {
            if (Dispatcher.HasThreadAccess)
            {
                UpdateStatus(strMessage, type);
            }
            else
            {
                var task = Dispatcher.RunAsync(CoreDispatcherPriority.Normal, () => UpdateStatus(strMessage, type));
            }
        }

        public async void OnConnectionReceived(StreamSocketListener sender, StreamSocketListenerConnectionReceivedEventArgs args)
        {
            // Don't need the listener anymore
            socketListener.Dispose();
            socketListener = null;
            commandCount = 0;

            await Dispatcher.RunAsync(CoreDispatcherPriority.Normal, () =>
            {
                if (dispatcherTimer != null)
                {
                    dispatcherTimer.Stop();
                }
                DispatcherTimerSetup((int)((commandCount++) / 27));
            });

            try
            {
                socket = args.Socket;
            }
            catch (Exception e)
            {
                await Dispatcher.RunAsync(CoreDispatcherPriority.Normal, () =>
                {
                    NotifyUser(e.Message, NotifyType.ErrorMessage);
                });
                return;
            }

            // Note - this is the supported way to get a Bluetooth device from a given socket
            var remoteDevice = await BluetoothDevice.FromHostNameAsync(socket.Information.RemoteHostName);

            writer = new DataWriter(socket.OutputStream);
            reader = new DataReader(socket.InputStream);
            remoteDisconnection = false;

            await Dispatcher.RunAsync(CoreDispatcherPriority.Normal, () =>
            {
                NotifyUser("Connected to client: " + remoteDevice.Name, NotifyType.StatusMessage);
            });

            // Infinite read buffer loop
            while (true)
            {
                try
                {
                    // Based on the protocol we've defined, the first uint is the size of the message
                    uint readLength = await reader.LoadAsync(sizeof(uint));

                    // Check if the size of the data is expected (otherwise the remote has already terminated the connection)
                    if (readLength < sizeof(uint))
                    {
                        remoteDisconnection = true;
                        break;
                    }
                    uint currentLength = reader.ReadUInt32();

                    await Dispatcher.RunAsync(CoreDispatcherPriority.Normal, () =>
                    {
                        dispatcherTimer.Stop();
                        DispatcherTimerSetup((int)((commandCount++) / 27));
                    });

                    // Load the rest of the message since you already know the length of the data expected.
                    readLength = await reader.LoadAsync(currentLength);

                    // Check if the size of the data is expected (otherwise the remote has already terminated the connection)
                    if (readLength < currentLength)
                    {
                        remoteDisconnection = true;
                        break;
                    }

                    string message = reader.ReadString(currentLength);

                    await Dispatcher.RunAsync(CoreDispatcherPriority.Normal, () =>
                    {
                        Incoming_text.Text = message;

                        if (message.Contains(PigeonThrowerCommands.MoveCommand))
                        {
                            SendMessage("Machine Moving...");
                        }

                        //calling the server functions to process input
                        string response = start.ParaseInput(message);
                        Response_Msg.Text = response;
                        SendMessage(response);

                        if (message.Contains(PigeonThrowerCommands.UpdateCommand))
                        {
                            UpdatePresets();
                        }
                    });
                }
                // Catch exception HRESULT_FROM_WIN32(ERROR_OPERATION_ABORTED).
                //catch (Exception ex) when ((uint)ex.HResult == 0x800703E3)
                //{
                //    await Dispatcher.RunAsync(CoreDispatcherPriority.Normal, () =>
                //    {
                //        Debug.WriteLine("Disconnect for caught exception");
                //        Disconnect();
                //        NotifyUser("CLIENT DISCONNECTED SUCCESSFULLY", NotifyType.StatusMessage);
                //    });
                //    break;
                //}
                catch (System.Exception e)
                {
                    NotifyUser(e.Message, NotifyType.ErrorMessage);
                    break;
                }
            }

            reader.DetachStream();
            if (remoteDisconnection)
            {
                await Dispatcher.RunAsync(CoreDispatcherPriority.Normal, () =>
                {
                    Disconnect();
                    NotifyUser("Client disconnected", NotifyType.StatusMessage);
                });
            }
        }

        #endregion Public Methods

        #region Protected Methods

        protected void OnPropertyChanged(string propertyName)
        {
            if (this.PropertyChanged != null)
            {
                this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion Protected Methods

        #region Private Methods

        private void AddToAppData()
        {
            var localSettings = ApplicationData.Current.LocalSettings;
            //var arr = localSettings.Values.ToArray();
            //for (int i = 0; i < localSettings.Values.Count; i++)
            //{
            //    System.Diagnostics.Debug.WriteLine(arr.ElementAt(i));
            //}

            if (localSettings.Values.Count < 31)
            {
                for (int i = 1; i <= 25; i++)
                {
                    this.presets[i] = new PigeonThrowerPosition(i, "Position " + i.ToString(), 0, 0, 0);
                    localSettings.Values["preset" + i.ToString()] = presets[i].ToString();
                }
            }
        }

        private void Calibrate_Button_Click(object sender, RoutedEventArgs e)
        {
            string msg = PigeonThrowerCommands.CalibrateCommand;
            Incoming_text.Text = msg;
            string response = start.ParaseInput(msg);
            Response_Msg.Text = response;
        }

        private void CyclePT_Button_Click(object sender, RoutedEventArgs e)
        {
            string msg = PigeonThrowerCommands.CycleCommand;
            Incoming_text.Text = msg;
            string response = start.ParaseInput(msg);
            Response_Msg.Text = response;
        }

        private void DisableJogButtonsExcept(int index)
        {
            for (int i = 0; i < 6; i++)
            {
                if (!(i == index))
                {
                    EnableJogButtons[i] = false;
                }
            }
            OnPropertyChanged("EnableJogButtons");
        }

        private void Disconnect()
        {
            if (rfcommProvider != null)
            {
                rfcommProvider.StopAdvertising();
                rfcommProvider = null;
            }

            if (socketListener != null)
            {
                socketListener.Dispose();
                socketListener = null;
            }

            if (writer != null)
            {
                writer.DetachStream();
                writer = null;
            }

            if (socket != null)
            {
                socket.Dispose();
                socket = null;
            }
            InitializeRfcommServer();
        }

        private void EnableAllJogButtons()
        {
            for (int i = 0; i < 6; i++)
            {
                EnableJogButtons[i] = true;
            }
            OnPropertyChanged("EnableJogButtons");
        }

        private void DispatcherTimerSetup(int commandNo)
        {
            dispatcherTimer = new DispatcherTimer();
            dispatcherTimer.Tick += dispatcherTimer_TickAsync;
            if (commandNo == 0)
            {
                dispatcherTimer.Interval = new TimeSpan(0, 0, 30);
            }
            else
            {
                dispatcherTimer.Interval = new TimeSpan(0, 2, 0);
            }
            dispatcherTimer.Start();
        }

        private async void dispatcherTimer_TickAsync(object sender, object e)
        {
            if (!remoteDisconnection)
            {
                await Dispatcher.RunAsync(CoreDispatcherPriority.Normal, () =>
                {
                    Disconnect();
                    NotifyUser("Client disconnected", NotifyType.StatusMessage);
                });

            }
        }

        private void ErrorLog_Button_Click(object sender, RoutedEventArgs e)
        {
            string msg = PigeonThrowerCommands.GetErrorLogCommand;
            Incoming_text.Text = msg;
            string response = start.ParaseInput(msg);
            Response_Msg.Text = response;
        }

        private void Home_Button_Click(object sender, RoutedEventArgs e)
        {
            string msg = PigeonThrowerCommands.HomeCommand;
            Incoming_text.Text = msg;
            string response = start.ParaseInput(msg);
            Response_Msg.Text = response;
        }

        private void LoadPresetsToView()
        {
            string presets = "\n";
            ApplicationDataContainer set = ApplicationData.Current.LocalSettings;
            if (set.Values.Count > 25)
            {
                for (int i = 1; i <= 25; i++)
                {
                    string[] values = (set.Values["preset" + i].ToString()).Split(',');
                    presets = values[0] + "\t\t" + values[1] + "\t\t" + values[2] + "\t\t" + values[3] + "\t\t" + values[4] + "\n";
                    PresetItems.Add(presets);
                }
            }
        }

        private void Move_Button_Click(object sender, RoutedEventArgs e)
        {
            string request = PigeonThrowerCommands.MoveCommand + "," + Pitch_Slider.Value.ToString() + "," +
                    Roll_Slider.Value.ToString() + "," + Pull_Slider.Value.ToString();
            Incoming_text.Text = request;
            string response = start.ParaseInput(request);
            Response_Msg.Text = response;
        }

        private void PF_Button_Click(object sender, RoutedEventArgs e)
        {
            var x = (SolidColorBrush)PF_Button.Background;
            if (x.Color == Windows.UI.Colors.White)
            {
                PF_Button.Background = new SolidColorBrush(Windows.UI.Colors.Green);
                DisableJogButtonsExcept(2);
            }
            else
            {
                PF_Button.Background = new SolidColorBrush(Windows.UI.Colors.White);
                EnableAllJogButtons();
            }

            string msg = PigeonThrowerCommands.JogCommand + "PF";
            Incoming_text.Text = msg;
            string response = start.ParaseInput(msg);
            Response_Msg.Text = response;
        }

        private void PR_Button_Click(object sender, RoutedEventArgs e)
        {
            var x = (SolidColorBrush)PR_Button.Background;
            if (x.Color == Windows.UI.Colors.White)
            {
                PR_Button.Background = new SolidColorBrush(Windows.UI.Colors.Green);
                DisableJogButtonsExcept(3);
            }
            else
            {
                PR_Button.Background = new SolidColorBrush(Windows.UI.Colors.White);
                EnableAllJogButtons();
            }

            string msg = PigeonThrowerCommands.JogCommand + "PR";
            Incoming_text.Text = msg;
            string response = start.ParaseInput(msg);
            Response_Msg.Text = response;
        }

        private void RF_Button_Click(object sender, RoutedEventArgs e)
        {
            var x = (SolidColorBrush)RF_Button.Background;
            if (x.Color == Windows.UI.Colors.White)
            {
                RF_Button.Background = new SolidColorBrush(Windows.UI.Colors.Green);
                DisableJogButtonsExcept(4);
            }
            else
            {
                RF_Button.Background = new SolidColorBrush(Windows.UI.Colors.White);
                EnableAllJogButtons();
            }

            string msg = PigeonThrowerCommands.JogCommand + "RF";
            Incoming_text.Text = msg;
            string response = start.ParaseInput(msg);
            Response_Msg.Text = response;
        }

        private void RR_Button_Click(object sender, RoutedEventArgs e)
        {
            var x = (SolidColorBrush)RR_Button.Background;
            if (x.Color == Windows.UI.Colors.White)
            {
                RR_Button.Background = new SolidColorBrush(Windows.UI.Colors.Green);
                DisableJogButtonsExcept(5);
            }
            else
            {
                RR_Button.Background = new SolidColorBrush(Windows.UI.Colors.White);
                EnableAllJogButtons();
            }

            string msg = PigeonThrowerCommands.JogCommand + "RR";
            Incoming_text.Text = msg;
            string response = start.ParaseInput(msg);
            Response_Msg.Text = response;
        }

        private async void SendMessage(string msg)
        {
            // There's no need to send a zero length message
            if (msg.Length != 0)
            {
                // Make sure that the connection is still up and there is a message to send
                if (socket != null)
                {
                    string message = msg;
                    writer.WriteUInt32((uint)message.Length);
                    writer.WriteString(message);
                    await writer.StoreAsync();
                }
                else
                {
                    NotifyUser("No clients conneted, please wait for a client to connect", NotifyType.ErrorMessage);
                }
            }
        }

        private void SF_Button_Click(object sender, RoutedEventArgs e)
        {
            var x = (SolidColorBrush)SF_Button.Background;
            if (x.Color == Windows.UI.Colors.White)
            {
                SF_Button.Background = new SolidColorBrush(Windows.UI.Colors.Green);
                DisableJogButtonsExcept(0);
            }
            else
            {
                SF_Button.Background = new SolidColorBrush(Windows.UI.Colors.White);
                EnableAllJogButtons();
            }

            string msg = PigeonThrowerCommands.JogCommand + "SF";
            Incoming_text.Text = msg;
            string response = start.ParaseInput(msg);
            Response_Msg.Text = response;
        }

        private void SR_Button_Click(object sender, RoutedEventArgs e)
        {
            var x = (SolidColorBrush)SR_Button.Background;
            if (x.Color == Windows.UI.Colors.White)
            {
                SR_Button.Background = new SolidColorBrush(Windows.UI.Colors.Green);
                DisableJogButtonsExcept(1);
            }
            else
            {
                SR_Button.Background = new SolidColorBrush(Windows.UI.Colors.White);
                EnableAllJogButtons();
            }

            string msg = PigeonThrowerCommands.JogCommand + "SR";
            Incoming_text.Text = msg;
            string response = start.ParaseInput(msg);
            Response_Msg.Text = response;
        }

        private void UpdatePresets()
        {
            string presets = "\n";
            ApplicationDataContainer set = ApplicationData.Current.LocalSettings;
            if (set.Values.Count > 1)
            {
                for (int i = 1; i <= 25; i++)
                {
                    string[] values = (set.Values["preset" + i].ToString()).Split(',');
                    presets = values[0] + "\t\t" + values[1] + "\t\t" + values[2] + "\t\t" + values[3] + "\t\t" + values[4] + "\n";
                    if (!PresetItems.Contains(presets))
                    {
                        PresetItems.RemoveAt(i - 1);
                        PresetItems.Insert(i - 1, presets);
                    }
                }
            }
        }

        //call function to write to log file
        private void UpdateStatus(string strMessage, NotifyType type)
        {
            start.WriteToFileAsync(type.ToString() +  " :: " +strMessage);
            System.Diagnostics.Debug.WriteLine("Log entry:  " + strMessage);
        }

        #endregion Private Methods
    }

    internal class BluetoothConstants
    {
        #region Public Fields

        // The value of the Service Name SDP attribute
        public const string SdpServiceName = "BLUETOOTH PIGEON THROWER SERVICE";

        // The Id of the Service Name SDP attribute
        public const UInt16 SdpServiceNameAttributeId = 0x100;

        // The SDP Type of the Service Name SDP attribute.
        // The first byte in the SDP Attribute encodes the SDP Attribute Type as follows :
        //    -  the Attribute Type size in the least significant 3 bits,
        //    -  the SDP Attribute Type value in the most significant 5 bits.
        public const byte SdpServiceNameAttributeType = (4 << 3) | 5;

        // The Chat Server's custom service Uuid: 34B1CF4D-1069-4AD6-89B6-E161D79BE4D8
        public static readonly Guid RfcommChatServiceUuid = Guid.Parse("34B1CF4D-1069-4AD6-89B6-E161D79BE4D8");

        #endregion Public Fields
    }
}