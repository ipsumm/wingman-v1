﻿using System;
using System.ComponentModel;
using System.Threading.Tasks;
using System.Threading;
using Windows.ApplicationModel.Background;
using Windows.Devices.Enumeration;
using Windows.Devices.Gpio;
using Windows.Devices.Spi;
using System.Threading;
using System.Diagnostics;
using Windows.Storage;

namespace IoTCoreDefaultApp
{
    public class StartupTask
    {
        #region Private Fields

        private const int Bad_Led = 21;
        private const int ESTOP = 18;
        private const int Good_Led = 20;
        private const int LSPH = 6;
        private const int LSPL = 5;
        private const int LSRH = 13;
        private const int LSRL = 12;
        private const int LSSH = 17;
        private const int LSSL = 16;
        private const byte MCP3002_CONFIG = 0x68;
        private const byte MCP3208_CONFIG = 0x06;
        private const int Motor1_F_Pin = 22;
        private const int Motor1_R_Pin = 23;
        private const int Motor2_F_Pin = 24;
        private const int Motor2_R_Pin = 25;
        private const int Motor3_F_Pin = 26;
        private const int Motor3_R_Pin = 27;
        private const Int32 SPI_CHIP_SELECT_LINE = 0;
        private const string SPI_CONTROLLER_NAME = "SPI0";
        private const int Spring_Enc = 19;
        private readonly byte[] MCP3008_CONFIG0 = { 0x01, 0x80, 0x00 };
        private readonly byte[] MCP3008_CONFIG1 = { 0x01, 0x90, 0x00 };
        private readonly byte[] MCP3008_CONFIG2 = { 0x01, 0xA0, 0x00 };
        private readonly byte[] MCP3008_CONFIG3 = { 0x01, 0xB0, 0x00 };
        private readonly byte[] MCP3008_CONFIG4 = { 0x01, 0xC0, 0x00 };
        private readonly byte[] MCP3008_CONFIG5 = { 0x01, 0xD0, 0x00 };
        private readonly byte[] MCP3008_CONFIG6 = { 0x01, 0xE0, 0x00 };
        private readonly byte[] MCP3008_CONFIG7 = { 0x01, 0xF0, 0x00 };
        private AdcDevice ADC_DEVICE = AdcDevice.MCP3008;
        private int[] AxesPotCurrValue = { 1025, 1025, 1025, 1025, 1025, 1025, 1025 };
        private int[] AxesPotReqValue = { 0, 0, 0 };

        private bool EStopActive = false;
        private GpioPin InEstop;
        private GpioPin InLSPH;
        private GpioPin InLSPL;
        private GpioPin InLSRH;
        private GpioPin InLSRL;
        private GpioPin InLSSH;
        private GpioPin InLSSL;
        private GpioPin InSpringEnc;
        private byte[][] MCP3008_CONFIG = new byte[8][];
        private GpioPin OutBadLED;
        private GpioPin OutGoodLED;
        private Timer periodicTimer;
        private long pitch_RawValue = 0;
        private GpioPin Pitch_Motor_On;
        private GpioPin Pitch_Dir;
        private long pitchMax_Calib = 0;
        private long pitchMax_Deg = 70;
        private long pitchMin_Calib = 0;
        private long pitchMin_Deg = 0;
        private long pitchPos_Deg = 0;
        private double Pitch_Calib_Factor = 1;

        private PigeonThrowerPosition[] presets;
        private long roll_RawValue = 0;
        private GpioPin Roll_Motor_On;
        private GpioPin Roll_Dir;
        private long rollMax_Calib = 0;
        private long rollMax_Deg = 40;
        private long rollMin_Calib = 0;
        private long rollMin_Deg = -40;
        private long rollPos_Deg = 0;
        private SpiDevice SpiADC;
        private long spring_CountsPerMM = 2;
        private long spring_Curr_Counts = 0;
        private GpioPin Spring_Motor_On;
        private GpioPin Spring_Dir;
        private long springMax_Calib = 300; //Value in Counts. Equal to springMax_Calib / spring_CountsPerMM millimeters
        private long springPos_MM = 0;
        private string status = "";
        private bool isCalibrated = false;
        private PigeonThrowerPosition currentPosition;
   

        #endregion Private Fields

        #region Public Events

        public StartupTask()
        {
            presets = new PigeonThrowerPosition[26];
            currentPosition = new PigeonThrowerPosition(0, "", 0, 0, 0);

            var vault = new Windows.Security.Credentials.PasswordVault();
            try
            {
                var credentialList = vault.FindAllByUserName("PT_ADMIN");
                credentialList[0].RetrievePassword();
                if (credentialList[0].Password == null)
                {
                    vault.Add(new Windows.Security.Credentials.PasswordCredential("My App", "PT_ADMIN", "admin"));
                }
            }
            catch (Exception e)
            {
                vault.Add(new Windows.Security.Credentials.PasswordCredential("My App", "PT_ADMIN", "admin"));
            }

            WriteToFileAsync("Application started");
        }

        public event PropertyChangedEventHandler PropertyChanged;

        #endregion Public Events

        #region Private Enums

        private enum AdcDevice
        { NONE, MCP3002, MCP3208, MCP3008 };

        #endregion Private Enums

        #region Public Properties

        public long Pitch_RawValue
        {
            get
            {
                return this.pitch_RawValue;
            }

            set
            {
                if (value != this.pitch_RawValue)
                {
                    this.pitch_RawValue = value;
                }
            }
        }

        public long PitchMax_Calib
        {
            get
            {
                return this.pitchMax_Calib;
            }

            set
            {
                if (value != this.pitchMax_Calib)
                {
                    this.pitchMax_Calib = value;
                }
            }
        }

        public long PitchMin_Calib
        {
            get
            {
                return this.pitchMin_Calib;
            }

            set
            {
                if (value != this.pitchMin_Calib)
                {
                    this.pitchMin_Calib = value;
                }
            }
        }

        public long PitchPos_Deg
        {
            get
            {
                return this.pitchPos_Deg;
            }

            set
            {
                if (value != this.pitchPos_Deg)
                {
                    this.pitchPos_Deg = value;
                }
            }
        }

        public long Roll_RawValue
        {
            get
            {
                return this.roll_RawValue;
            }

            set
            {
                if (value != this.roll_RawValue)
                {
                    this.roll_RawValue = value;
                }
            }
        }

        public long RollMax_Calib
        {
            get
            {
                return this.rollMax_Calib;
            }

            set
            {
                if (value != this.rollMax_Calib)
                {
                    this.rollMax_Calib = value;
                }
            }
        }

        public long RollMin_Calib
        {
            get
            {
                return this.rollMin_Calib;
            }

            set
            {
                if (value != this.rollMin_Calib)
                {
                    this.rollMin_Calib = value;
                }
            }
        }

        public long RollPos_Deg
        {
            get
            {
                return this.rollPos_Deg;
            }

            set
            {
                if (value != this.rollPos_Deg)
                {
                    this.rollPos_Deg = value;
                }
            }
        }

        public long Spring_Curr_Counts
        {
            get
            {
                return this.spring_Curr_Counts;
            }

            set
            {
                if (this.spring_Curr_Counts != value)
                {
                    this.spring_Curr_Counts = value;
                }
            }
        }

        public long SpringPos_MM
        {
            get
            {
                return this.springPos_MM;
            }

            set
            {
                if (value != this.springPos_MM)
                {
                    this.springPos_MM = value;
                }
            }
        }

        #endregion Public Properties

        #region Public Methods

        public async void WriteToFileAsync(string log)
        {
            StorageFolder folder = ApplicationData.Current.LocalFolder;
            try
            {
                StorageFile file = await folder.CreateFileAsync("log.txt", CreationCollisionOption.OpenIfExists);
                //if (log.Contains("Application started"))
                //{
                //    await file.DeleteAsync();
                //}
                //file = await folder.CreateFileAsync("log.txt", CreationCollisionOption.OpenIfExists);
                await FileIO.AppendTextAsync(file, "\r\n" + log + "\t\t" + System.DateTime.Now.ToString());
            }
            catch(Exception e)
            {
                Debug.WriteLine("Error writing to log file");
            }
        }

        public async Task<string> ReadFromLogFileAsync()
        {
            string logFileContent = "";
            StorageFolder folder = ApplicationData.Current.LocalFolder;
            try
            {
                StorageFile file = await folder.GetFileAsync("log.txt").AsTask().ConfigureAwait(false);
                logFileContent = await FileIO.ReadTextAsync(file).AsTask().ConfigureAwait(false);
            }
            catch (Exception e)
            {
                logFileContent = "Error reading log file: " + e.Message.ToString();
            }
            return logFileContent;
        }

        public int convertToInt([System.Runtime.InteropServices.WindowsRuntime.ReadOnlyArray()] byte[] data)
        {
            int result = 0;
            switch (ADC_DEVICE)
            {
                case AdcDevice.MCP3002:
                    result = data[0] & 0x03;
                    result <<= 8;
                    result += data[1];
                    break;

                case AdcDevice.MCP3208:
                    result = data[1] & 0x0F;
                    result <<= 8;
                    result += data[2];
                    break;

                case AdcDevice.MCP3008:
                    result = data[1] & 0x03;
                    result <<= 8;
                    result += data[2];
                    break;
            }
            return result;
        }

        public void ReadADC()
        {
            byte[] readBuffer = new byte[3]; /* Buffer to hold read data*/
            byte[] writeBuffer = new byte[3] { 0x00, 0x00, 0x00 };

            MCP3008_CONFIG[0] = MCP3008_CONFIG0;
            MCP3008_CONFIG[1] = MCP3008_CONFIG1;
            MCP3008_CONFIG[2] = MCP3008_CONFIG2;
            MCP3008_CONFIG[3] = MCP3008_CONFIG3;
            MCP3008_CONFIG[4] = MCP3008_CONFIG4;
            MCP3008_CONFIG[5] = MCP3008_CONFIG5;
            MCP3008_CONFIG[6] = MCP3008_CONFIG6;
            MCP3008_CONFIG[7] = MCP3008_CONFIG7;

            //read three analog inputs over one channel
            for (int i = 0; i < 3; i++)
            {
                writeBuffer = MCP3008_CONFIG[i];
                SpiADC.TransferFullDuplex(writeBuffer, readBuffer); /* Read data from the ADC                           */
                AxesPotCurrValue[i] = convertToInt(readBuffer);                /* Convert the returned bytes into an integer value */
            }

            Pitch_RawValue = AxesPotCurrValue[0];
            Roll_RawValue = AxesPotCurrValue[1];

        }

        public string JogFunctions(string data)
        {
            string result = "";

            //move spring axis forward
            if (data.Contains("SF"))
            {
                Spring_Dir.Write(GpioPinValue.Low);
                if (Spring_Motor_On.Read() == GpioPinValue.Low)
                {
                    Spring_Motor_On.Write(GpioPinValue.High);
                }
                else
                {
                    Spring_Motor_On.Write(GpioPinValue.Low);
                }
                result = "SPRING FORWARD";
            }

            //move spring axis in reverse direction
            else if (data.Contains("SR"))
            {
                if (Spring_Motor_On.Read() == GpioPinValue.Low)
                {
                    Spring_Motor_On.Write(GpioPinValue.High);
                    Spring_Dir.Write(GpioPinValue.High);
                }
                else
                {
                    Spring_Motor_On.Write(GpioPinValue.Low);
                    Spring_Dir.Write(GpioPinValue.Low);
                }
                result = "SPRING REVERSE";
            }

            //move pitch axis in forward direction
            else if (data.Contains("PF"))
            {
                Pitch_Dir.Write(GpioPinValue.Low);
                if (Pitch_Motor_On.Read() == GpioPinValue.Low)
                {
                    Pitch_Motor_On.Write(GpioPinValue.High);
                }
                else
                {
                    Pitch_Motor_On.Write(GpioPinValue.Low);
                }
                result = "PITCH FORWARD";
            }

            //move pitch axis in reverse direction
            else if (data.Contains("PR"))
            {
                if (Pitch_Motor_On.Read() == GpioPinValue.Low)
                {
                    Pitch_Motor_On.Write(GpioPinValue.High);
                    Pitch_Dir.Write(GpioPinValue.High);
                }
                else
                {
                    Pitch_Motor_On.Write(GpioPinValue.Low);
                    Pitch_Dir.Write(GpioPinValue.Low);
                }
                result = "PITCH REVERSE";
            }

            //move roll axis in forward direction
            else if (data.Contains("RF"))
            {
                Roll_Dir.Write(GpioPinValue.Low);
                if (Roll_Motor_On.Read() == GpioPinValue.Low)
                {
                    Roll_Motor_On.Write(GpioPinValue.High);
                }
                else
                {
                    Roll_Motor_On.Write(GpioPinValue.Low);
                }
                result = "ROLL FORWARD";
            }

            //move roll axis in reverse direction
            else if (data.Contains("RR"))
            {
                if (Roll_Motor_On.Read() == GpioPinValue.Low)
                {
                    Roll_Motor_On.Write(GpioPinValue.High);
                    Roll_Dir.Write(GpioPinValue.High);
                }
                else
                {
                    Roll_Motor_On.Write(GpioPinValue.Low);
                    Roll_Dir.Write(GpioPinValue.Low);
                }
                result = "ROLL REVERSE";
            }

            return result;
        }

        public string ParaseInput(string data)
        {
            var localSettings = ApplicationData.Current.LocalSettings;
            string output = "";

            //reset the machine
            if (data.StartsWith(PigeonThrowerCommands.ResetCommand))
            {
                if (!EStopActive)
                {
                    this.MoveTo(0, 0, 0);
                    output = "MACHINE RESET";
                }

                else
                {
                    output = "SWITCH OFF EMERGENCY STOP TO RESET MACHINE";
                }
            }

            //calibrate machine
            else if (data.StartsWith(PigeonThrowerCommands.CalibrateCommand))
            {
                if (!EStopActive)
                {
                    this.Calib_All();
                    output = "MACHINE CALIBRATED";
                }
                else
                {
                    output = "SWITCH OFF EMERGENCY STOP AND RESET MACHINE";
                }
            }

            //run full cyclye test
            else if (data.StartsWith(PigeonThrowerCommands.CycleCommand))
            {
                if (!EStopActive)
                {
                    this.PT_Test();
                    output = "  CYCLEPT";
                }
                else
                {
                    output = "SWITCH OFF EMERGENCY STOP AND RESET MACHINE";
                }
            }

            //move machine to given position
            else if (data.StartsWith(PigeonThrowerCommands.MoveCommand))
            {
                if (!EStopActive)
                {
                    string[] values = data.Split(',');
                    long r = Convert.ToInt64(values[1]);
                    long p = Convert.ToInt64(values[2]);
                    long s = Convert.ToInt64(values[3]);
                    this.MoveTo(r, p, s);
                    output = "MACHINE MOVED";
                }
                else
                {
                    output = "SWITCH OFF EMERGENCY STOP AND RESET MACHINE";
                }
            }

            //save specified presets to machine
            else if (data.StartsWith(PigeonThrowerCommands.SavePresetCommand))
            {
                string[] values = data.Split(',');
                int presetNum = localSettings.Values.Count - 1;
                if (presetNum <= 25 && presetNum >= 0)
                {
                    this.presets[presetNum] = new PigeonThrowerPosition(presetNum, values[4], Convert.ToInt64(values[1]),
                        Convert.ToInt64(values[2]), Convert.ToInt64(values[3]));
                    localSettings.Values["preset" + presetNum.ToString()] = presets[presetNum].ToString();
                    output = "VALUE OF PRESET " + presetNum + " SAVED";
                }
                else
                    output = "COULD NOT SAVE AS SETTING EXCEEDS MAX NUMBER OF PRESETS";
            }

            //update a saved preset value
            else if (data.StartsWith(PigeonThrowerCommands.UpdateCommand))
            {
                string[] values = data.Split(',');
                int presetNum = Convert.ToInt32(values[1]);
                this.presets[presetNum] = new PigeonThrowerPosition(presetNum, values[5], Convert.ToInt64(values[2]),
                    Convert.ToInt64(values[3]), Convert.ToInt64(values[4]));
                localSettings.Values["preset" + presetNum.ToString()] = presets[presetNum].ToString();
                output = "UPDATE PRESET " + presetNum;
            }

            //return specified preset
            else if (data.StartsWith(PigeonThrowerCommands.GetPresetCommand))
            {
                string[] values = data.Split(' ');
                if (Convert.ToInt32(values[2]) >= 0 && Convert.ToInt32(values[2]) <= 25)
                    output = "PRESET IS:" + localSettings.Values[values[1] + values[2]].ToString();
                else
                    output = "PRESET" + values[2] + " IS NOT DEFINED.";
            }

            //save login information of new registered user
            else if (data.StartsWith(PigeonThrowerCommands.SavePasswordCommand))
            {
                string[] pass = data.Split(' ', ',');
                var vault = new Windows.Security.Credentials.PasswordVault();
                if (data.Contains("ADMIN"))
                    vault.Add(new Windows.Security.Credentials.PasswordCredential("My App", "PT_ADMIN", pass[4]));
                if (data.Contains("USER"))
                    vault.Add(new Windows.Security.Credentials.PasswordCredential("My App", "PT_USER", pass[4]));
                output = "PASSWORD UPDATED";
            }

            //authenticate user credentials
            else if (data.StartsWith(PigeonThrowerCommands.LoginCommand))
            {
                string[] credentials = data.Split(' ');
                output = GetCredentialFromLocker(credentials[1]);
            }

            //read error logs from the controller
            else if (data.StartsWith(PigeonThrowerCommands.GetErrorLogCommand))
            {
                var res = ReadFromLogFileAsync();
                string s = res.Result.ToString();
                output = PigeonThrowerCommands.DisplayErrorLogCommand + ":\n" + s;
            }

            //calls the jog method
            else if (data.StartsWith(PigeonThrowerCommands.JogCommand))
            {
                output = JogFunctions(data);
            }

            else
            {
                output = "COMMAND NOT RECOGNIZED";
            }

            return output;
        }

        // authenticate the user credentials
        private string GetCredentialFromLocker(string password)
        {
            var vault = new Windows.Security.Credentials.PasswordVault();
            string output = "USER AUTHENTICATION FAILED";

            try
            {
                var credentialList = vault.FindAllByUserName("PT_ADMIN");
                credentialList[0].RetrievePassword();
                if (credentialList[0].Password.Equals(password))
                {
                    output = "USER AUTHENTICATION SUCCESSFUL: ADMIN IS LOGGED IN";
                }
                credentialList = vault.FindAllByUserName("PT_USER");
                credentialList[0].RetrievePassword();
                if (credentialList[0].Password.Equals(password))
                {
                    output = "USER AUTHENTICATION SUCCESSFUL: USER IS LOGGED IN";
                }
            }
            catch (Exception e)
            {
                Debug.Write(e);
            }
            return output;
        }




        public async void InitAll()
        {
            try
            {
                InitGpio();         /* Initialize GPIO to toggle the LED                          */
                await InitSPI();    /* Initialize the SPI bus for communicating with the ADC      */
                isCalibrated = false;
            }
            catch (Exception ex)
            {
                status = ex.Message;
                return;
            }

            var localSettings = ApplicationData.Current.LocalSettings;
            //localSettings.Values["Pitch_High"] = 900;
            if (localSettings.Values["Roll_Low"] == null)
            {
                localSettings.Values["Roll_Low"] = 1;
            }
            if (localSettings.Values["Roll_High"] == null)
            {
                localSettings.Values["Roll_High"] = 2;
            }
            if (localSettings.Values["Pitch_Low"] == null)
            {
                localSettings.Values["Pitch_Low"] = 1;
            }
            if (localSettings.Values["Pitch_High"] == null)
            {
                localSettings.Values["Pitch_High"] = 2;
            }

            //for (int i = 1; i <= 25; i++)
            //{
            //    string index = "preset" + i.ToString();
            //    if (localSettings.Values[index] == null)
            //    {
            //        localSettings.Values[index] = index + ",0,0,0";
            //    }
            //}

            RollMin_Calib = Convert.ToInt16(localSettings.Values["Roll_Low"]);
            RollMax_Calib = Convert.ToInt16(localSettings.Values["Roll_High"]);
            PitchMin_Calib = Convert.ToInt64(localSettings.Values["Pitch_Low"]);
            PitchMax_Calib = Convert.ToInt64(localSettings.Values["Pitch_High"]);

            Set_Pitch_Calib_Factor();

            //for (int i = 1; i <= 25; i++)
            //{
            //    string presettot = (string)localSettings.Values["preset" + i.ToString()];
            //    string[] presetsplit = presettot.Split(',');
            //    this.presets[i] = new PigeonThrowerPosition(Convert.ToInt32(presetsplit[0]), presetsplit[1], Convert.ToInt64(presetsplit[2]), Convert.ToInt64(presetsplit[3]), Convert.ToInt64(presetsplit[4]));
            //}

            /* Now that everything is initialized, create a timer so we read data every 500mS */
            periodicTimer = new Timer(this.Timer_Tick, null, 0, 500);

            status = "Status: Running";
            Debug.WriteLine(DateTime.Now.ToString());

        }

        #endregion Public Methods

        #region Private Methods

        private void TaskInstance_Canceled(IBackgroundTaskInstance sender, BackgroundTaskCancellationReason reason)
        {
            //  throw new NotImplementedException();
            Debug.WriteLine(reason.ToString());
        }

        private void Calib_All()
        {
            if (InEstop.Read() == GpioPinValue.High)
            {
                Spring_Calib();
            }

            if (InEstop.Read() == GpioPinValue.High)
            {
                Pitch_Calib();
            }

            if (InEstop.Read() == GpioPinValue.High)
            {
                Roll_Calib();
            }

            if (InEstop.Read() == GpioPinValue.High)
            {
                isCalibrated = true;
                currentPosition = new PigeonThrowerPosition(0, "", 5, 0, 0);
            }
        }

        private void InEstop_ValueChanged(GpioPin sender, GpioPinValueChangedEventArgs args)
        {
            if (sender.Read() == GpioPinValue.Low)
            {
                Debug.WriteLine("Estop activated");
                this.EStopActive = true;
                Stop_All_Axes();
            }
            else
            {
                this.EStopActive = false;
                Debug.WriteLine("Estop reset");
            }
        }

        private void InitGpio()
        {
            var gpio = GpioController.GetDefault();

            /* Show an error if there is no GPIO controller */
            if (gpio == null)
            {
                throw new Exception("There is no GPIO controller on this device");
            }

            InLSPL = gpio.OpenPin(LSPL);
            InLSPH = gpio.OpenPin(LSPH);
            InLSRL = gpio.OpenPin(LSRL);
            InLSRH = gpio.OpenPin(LSRH);
            InLSSL = gpio.OpenPin(LSSL);
            InLSSH = gpio.OpenPin(LSSH);
            InEstop = gpio.OpenPin(ESTOP);
            InSpringEnc = gpio.OpenPin(Spring_Enc);

            OutGoodLED = gpio.OpenPin(Good_Led);
            OutBadLED = gpio.OpenPin(Bad_Led);
            Pitch_Motor_On = gpio.OpenPin(Motor1_F_Pin);
            Pitch_Dir = gpio.OpenPin(Motor1_R_Pin);
            Roll_Motor_On = gpio.OpenPin(Motor2_F_Pin);
            Roll_Dir = gpio.OpenPin(Motor2_R_Pin);
            Spring_Motor_On = gpio.OpenPin(Motor3_F_Pin);
            Spring_Dir = gpio.OpenPin(Motor3_R_Pin);


            /* GPIO state is initially undefined, so we assign a default value before enabling as output */
            OutGoodLED.Write(GpioPinValue.High);
            OutBadLED.Write(GpioPinValue.High);
            Pitch_Motor_On.Write(GpioPinValue.Low);
            Pitch_Dir.Write(GpioPinValue.Low);
            Roll_Motor_On.Write(GpioPinValue.Low);
            Roll_Dir.Write(GpioPinValue.Low);
            Spring_Motor_On.Write(GpioPinValue.Low);
            Spring_Dir.Write(GpioPinValue.Low);

            InLSPL.SetDriveMode(GpioPinDriveMode.InputPullDown);
            InLSPH.SetDriveMode(GpioPinDriveMode.InputPullDown);
            InLSRL.SetDriveMode(GpioPinDriveMode.InputPullDown);
            InLSRH.SetDriveMode(GpioPinDriveMode.InputPullDown);
            InLSSL.SetDriveMode(GpioPinDriveMode.InputPullDown);
            InLSSH.SetDriveMode(GpioPinDriveMode.InputPullDown);
            InEstop.SetDriveMode(GpioPinDriveMode.Input);
            InSpringEnc.SetDriveMode(GpioPinDriveMode.InputPullUp);

            OutGoodLED.SetDriveMode(GpioPinDriveMode.Output);
            OutBadLED.SetDriveMode(GpioPinDriveMode.Output);
            Pitch_Motor_On.SetDriveMode(GpioPinDriveMode.Output);
            Pitch_Dir.SetDriveMode(GpioPinDriveMode.Output);
            Roll_Motor_On.SetDriveMode(GpioPinDriveMode.Output);
            Roll_Dir.SetDriveMode(GpioPinDriveMode.Output);
            Spring_Motor_On.SetDriveMode(GpioPinDriveMode.Output);
            Spring_Dir.SetDriveMode(GpioPinDriveMode.Output);

            OutGoodLED.Write(GpioPinValue.Low);

            InEstop.ValueChanged += InEstop_ValueChanged;
        }

        private async Task InitSPI()
        {
            try
            {
                var settings = new SpiConnectionSettings(SPI_CHIP_SELECT_LINE);
                settings.ClockFrequency = 1000000;   /* 1MHz clock rate                                        */

                settings.Mode = SpiMode.Mode0;      /* The ADC expects idle-low clock polarity so we use Mode0  */

                string spiAqs = SpiDevice.GetDeviceSelector(SPI_CONTROLLER_NAME);
                var deviceInfo = await DeviceInformation.FindAllAsync(spiAqs);
                SpiADC = await SpiDevice.FromIdAsync(deviceInfo[0].Id, settings);
            }

            /* If initialization fails, display the exception and stop running */
            catch (Exception ex)
            {
                throw new Exception("SPI Initialization Failed", ex);
            }
        }

        private void MainPage_Unloaded(object sender, object args)
        {
            /* It's good practice to clean up after we're done */
            if (SpiADC != null)
            {
                SpiADC.Dispose();
            }

            if (OutGoodLED != null)
            {
                OutGoodLED.Dispose();
            }
        }

        private void MoveTo(long p, long r, long s)
        {
            if (!isCalibrated)
            {
                Calib_All();
            }

            if (!(currentPosition.Pitch == p && currentPosition.Roll == r && currentPosition.Pull == s))
            {
                if (InEstop.Read() == GpioPinValue.High)
                {
                    Spring_MoveTo(s);
                }

                if (InEstop.Read() == GpioPinValue.High)
                {
                    Pitch_MoveTo(p);
                }

                if (InEstop.Read() == GpioPinValue.High)
                {
                    Roll_MoveTo(r);
                }
                currentPosition = new PigeonThrowerPosition(0, "", p, r, s);
            }
            Debug.WriteLine("Current position is : " + PitchPos_Deg + " , " + RollPos_Deg + " , " + SpringPos_MM);
        }

        private void Pitch_Calib()
        {
            var localSettings = Windows.Storage.ApplicationData.Current.LocalSettings;
            while (InLSPH.Read() == GpioPinValue.High && InEstop.Read() == GpioPinValue.High)
            {
                Pitch_Dir.Write(GpioPinValue.Low);
                Pitch_Motor_On.Write(GpioPinValue.High);
            }
            Stop_All_Axes();

            if (InEstop.Read() == GpioPinValue.High)
            {
                localSettings.Values["Pitch_High"] = pitch_RawValue;
            }
            while (InLSPL.Read() == GpioPinValue.High && InEstop.Read() == GpioPinValue.High)
            {
                Pitch_Dir.Write(GpioPinValue.High);
                Pitch_Motor_On.Write(GpioPinValue.High);
            }
            // Debug.WriteLine("PitchCalib WhileExit2");
            Stop_All_Axes();

            if (InEstop.Read() == GpioPinValue.High)
            {
                localSettings.Values["Pitch_Low"] = pitch_RawValue;
            }

            PitchMin_Calib = Convert.ToInt64(localSettings.Values["Pitch_Low"]);
            PitchMax_Calib = Convert.ToInt64(localSettings.Values["Pitch_High"]);

            Set_Pitch_Calib_Factor();

            Update_Pitch_CurrPos();

            Update_Roll_CurrPos();

            Pitch_MoveTo(5);
        }

        private void Pitch_MoveTo(long DegPos)
        {
            Debug.WriteLine("Entering Pitch_MoveTo()");
            if (DegPos <= pitchPos_Deg && InLSPL.Read() == GpioPinValue.High && InEstop.Read() == GpioPinValue.High)
            {
                Pitch_Dir.Write(GpioPinValue.High);
                Pitch_Motor_On.Write(GpioPinValue.High);

                while (InLSPL.Read() == GpioPinValue.High && DegPos <= pitchPos_Deg && InEstop.Read() == GpioPinValue.High)
                {
                }
            }
            else if (InLSPH.Read() == GpioPinValue.High && InEstop.Read() == GpioPinValue.High)
            {
                Pitch_Dir.Write(GpioPinValue.Low);
                Pitch_Motor_On.Write(GpioPinValue.High);
                while (InLSPH.Read() == GpioPinValue.High && DegPos > pitchPos_Deg && InEstop.Read() == GpioPinValue.High)
                {
                }
            }
            Pitch_Dir.Write(GpioPinValue.Low);
            Pitch_Motor_On.Write(GpioPinValue.Low);
            Stop_All_Axes();
            Debug.WriteLine("Exiting Pitch_MoveTo()");
        }

        private void Roll_Calib()
        {
            var localSettings = Windows.Storage.ApplicationData.Current.LocalSettings;
            while (InLSRH.Read() == GpioPinValue.High && InEstop.Read() == GpioPinValue.High)
            {
                Roll_Dir.Write(GpioPinValue.Low);
                Roll_Motor_On.Write(GpioPinValue.High);
            }
            //Debug.WriteLine("RollCalib WhileExit1");
            Stop_All_Axes();
            if (InEstop.Read() == GpioPinValue.High)
            {
                localSettings.Values["Roll_Low"] = roll_RawValue;
            }
            while (InLSRL.Read() == GpioPinValue.High && InEstop.Read() == GpioPinValue.High)
            {
                Roll_Dir.Write(GpioPinValue.High);
                Roll_Motor_On.Write(GpioPinValue.High);
            }
            //Debug.WriteLine("RollCalib WhileExit2");

            Stop_All_Axes();

            if (InEstop.Read() == GpioPinValue.High)
            {
                localSettings.Values["Roll_High"] = roll_RawValue;
            }

            RollMin_Calib = Convert.ToInt16(localSettings.Values["Roll_Low"]);
            RollMax_Calib = Convert.ToInt16(localSettings.Values["Roll_High"]);

            Debug.WriteLine("RollCalib Low : " + RollMin_Calib);
            Debug.WriteLine("RollCalib High : " + RollMax_Calib);

            Update_Pitch_CurrPos();

            Update_Roll_CurrPos();

            Roll_MoveTo(0);

        }

        private void Pitch_TestFB()
        {
            while (InLSPH.Read() == GpioPinValue.High && InEstop.Read() == GpioPinValue.High)
            {
                Pitch_Dir.Write(GpioPinValue.Low);
                Pitch_Motor_On.Write(GpioPinValue.High);
            }
            //Debug.WriteLine("PitchCalib WhileExit1");
            Stop_All_Axes();
        }

        private void Pitch_TestBF()
        {
            while (InLSPL.Read() == GpioPinValue.High && InEstop.Read() == GpioPinValue.High)
            {
                Pitch_Dir.Write(GpioPinValue.High);
                Pitch_Motor_On.Write(GpioPinValue.High);
            }
            //Debug.WriteLine("PitchCalib WhileExit2");
            Stop_All_Axes();
        }

        private void Roll_TestLR()
        {
            while (InLSRL.Read() == GpioPinValue.High && InEstop.Read() == GpioPinValue.High)
            {
                Roll_Dir.Write(GpioPinValue.Low);
                Roll_Motor_On.Write(GpioPinValue.High);
            }
            //Debug.WriteLine("RollCalib WhileExit1");
            Stop_All_Axes();
        }

        private void Roll_TestRL()
        {
            while (InLSRH.Read() == GpioPinValue.High && InEstop.Read() == GpioPinValue.High)
            {
                Roll_Dir.Write(GpioPinValue.High);
                Roll_Motor_On.Write(GpioPinValue.High);
            }
            //Debug.WriteLine("RollCalib WhileExit2");

            Stop_All_Axes();
        }

        private void PT_Test()
        {
            int a = 0;
            while (true)
            {
                Debug.WriteLine("Cycles ", a.ToString());
                a = a + 1;
                Roll_TestLR();
                Pitch_TestFB();
                Roll_TestRL();
                Pitch_TestBF();
                Task.Delay(60000).Wait();
            }
        }

        private void Roll_MoveTo(long DegPos)
        {
            if (DegPos <= rollPos_Deg && InLSRH.Read() == GpioPinValue.High && InEstop.Read() == GpioPinValue.High)
            {
                //Debug.WriteLine("Entering Roll Move to while loop 1");
                Roll_Dir.Write(GpioPinValue.Low);
                Roll_Motor_On.Write(GpioPinValue.High);

                while (InLSRH.Read() == GpioPinValue.High && DegPos <= rollPos_Deg && InEstop.Read() == GpioPinValue.High)
                {
                }
                //Debug.WriteLine("RollMoveTo WhileExit1");
            }
            else if (InLSRL.Read() == GpioPinValue.High && InEstop.Read() == GpioPinValue.High)
            {
                Roll_Dir.Write(GpioPinValue.High);
                Roll_Motor_On.Write(GpioPinValue.High);
                while (InLSRL.Read() == GpioPinValue.High && DegPos > rollPos_Deg && InEstop.Read() == GpioPinValue.High)
                {
                }
                //Debug.WriteLine("RollMoveTo WhileExit2");
            }

            Roll_Dir.Write(GpioPinValue.Low);
            Roll_Motor_On.Write(GpioPinValue.Low);
            Stop_All_Axes();


            if (Math.Abs(DegPos - rollPos_Deg) > 5 && InLSRL.Read() == GpioPinValue.High && InLSRH.Read() == GpioPinValue.High && InEstop.Read() == GpioPinValue.High)
            {
                Roll_MoveTo(DegPos);
            }
        }

        private void Socket_OnError(string message)
        {
        }

        private void Spring_Calib()
        {
            Spring_Dir.Write(GpioPinValue.High);
            Spring_Motor_On.Write(GpioPinValue.High);
            while (InLSSL.Read() == GpioPinValue.High && InEstop.Read() == GpioPinValue.High)
            {
            }

            Stop_All_Axes();
            if (InEstop.Read() == GpioPinValue.High)
            {
                // Spring_Curr_Counts = springMax_Calib;
                Spring_Curr_Counts = 0;
                Spring_MoveTo(10);
            }
            Stop_All_Axes();
        }

        private void Spring_MoveTo(long mmPos)
        {
            //Debug.WriteLine("Entered Spring_MoveTo()");
            long ToPosition = mmPos * spring_CountsPerMM;
            if (ToPosition > springMax_Calib)
            {
                ToPosition = springMax_Calib;
            }
            if (ToPosition < 0)
            {
                Debug.WriteLine("Cannot command a spring move to less than 0.");
                return;
            }
            int pulse = 0;
            if (ToPosition > Spring_Curr_Counts && InEstop.Read() == GpioPinValue.High)
            {
                Spring_Dir.Write(GpioPinValue.Low);
                Spring_Motor_On.Write(GpioPinValue.High);
                int i = 0;
                while (ToPosition >= Spring_Curr_Counts && InEstop.Read() == GpioPinValue.High)
                {
                    if (pulse == 0 && InSpringEnc.Read() == GpioPinValue.High)
                    {
                        Spring_Curr_Counts = Spring_Curr_Counts + 1;
                        pulse = 1;
                        Debug.WriteLine("i = " + (i++) + "  Spring Count : " + Spring_Curr_Counts);
                    }
                    if (pulse == 1 && InSpringEnc.Read() == GpioPinValue.Low)
                    {
                        pulse = 0;
                    }
                }
            }
            else if (InLSSH.Read() == GpioPinValue.High && InEstop.Read() == GpioPinValue.High)
            {
                Spring_Dir.Write(GpioPinValue.High);
                Spring_Motor_On.Write(GpioPinValue.High);
                int j = 0;
                while (InLSSL.Read() == GpioPinValue.High && ToPosition < Spring_Curr_Counts && InEstop.Read() == GpioPinValue.High)
                {
                    if (pulse == 0 && InSpringEnc.Read() == GpioPinValue.High && InLSSH.Read() == GpioPinValue.High)
                    {
                        Spring_Curr_Counts = Spring_Curr_Counts - 1;
                        pulse = 1;
                        Debug.WriteLine("j = " + (j++) + "  Spring Count : " + Spring_Curr_Counts);
                    }
                    if (pulse == 1 && InSpringEnc.Read() == GpioPinValue.Low)
                    {
                        pulse = 0;
                    }
                }
                // Recalibrate spring if limit is hit
                //if (InLSSL.Read() == GpioPinValue.Low)
                //{
                //    Spring_Curr_Counts = springMax_Calib;
                //}
                //Debug.WriteLine("SpringMoveTo WhileExit2");
            }
            Debug.WriteLine("Spring Max : " + springMax_Calib);
            Stop_All_Axes();
        }

        private void Stop_All_Axes()
        {
            Pitch_Motor_On.Write(GpioPinValue.Low);
            Pitch_Dir.Write(GpioPinValue.Low);
            Roll_Motor_On.Write(GpioPinValue.Low);
            Roll_Dir.Write(GpioPinValue.Low);
            Spring_Motor_On.Write(GpioPinValue.Low);
            Spring_Dir.Write(GpioPinValue.Low);

            if (InEstop.Read() == GpioPinValue.High)
            {
                Debug.WriteLine("Wait started");
                Task.Delay(200).Wait();
                Debug.WriteLine("Wait ended");
            }
        }

        private void Timer_Tick(object state)
        {
            ReadADC();
            Update_Pitch_CurrPos();
            Update_Roll_CurrPos();
            Update_Spring_Pos();
        }

        private void UISafePropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        private void Update_Pitch_CurrPos()
        {
            PitchPos_Deg = Convert.ToInt32((pitch_RawValue - pitchMin_Calib) * Pitch_Calib_Factor);
        }

        private void Set_Pitch_Calib_Factor()
        {
            double calib_factorn = (pitchMax_Deg - pitchMin_Deg);
            double calib_factord = (pitchMax_Calib - pitchMin_Calib);
            Pitch_Calib_Factor = calib_factorn / calib_factord;
        }

        private void Update_Roll_CurrPos()
        {
            double degrees = 0;
            double calib_factor;
            double datum_pos;
            double calib_factorn = (rollMax_Deg - rollMin_Deg);
            double calib_factord = (rollMax_Calib - rollMin_Calib);
            calib_factor = calib_factorn / calib_factord;
            datum_pos = roll_RawValue - ((rollMin_Calib + rollMax_Calib) / 2);
            degrees = datum_pos * calib_factor;
            RollPos_Deg = Convert.ToInt32(degrees);
        }

        private void Update_Spring_Pos()
        {
            SpringPos_MM = Spring_Curr_Counts / spring_CountsPerMM;
        }

        #endregion Private Methods
    }
}
