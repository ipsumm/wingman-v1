﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IoTCoreDefaultApp
{
    public class PigeonThrowerCommands
    {
        public static string HomeCommand = "MOVE,0,0,0";
        public static string MoveCommand = "MOVE";
        public static string ResetCommand = "RESET";
        public static string LoginCommand = "LOGIN";
        public static string GetPresetCommand = "GET PRESET";
        public static string CalibrateCommand = "CALIBRATE";
        public static string CycleCommand = "CYCLEPT";
        public static string SavePresetCommand = "SAVE PRESET ";
        public static string UpdateCommand = "UPDATE PRESET";
        public static string SavePasswordCommand = "SAVE PASSWORD";
        public static string UserPrivilegeCommand = "USER PRIVILEGE";
        public static string DisplayPresetCommand = "PRESET";
        public static string ValidateUserCommand = "USER AUTHENTICATION";
        public static string GetErrorLogCommand = "GET ERROR LOG";
        public static string DisplayErrorLogCommand = "LOG FILE CONTAINS";
        public static string JogCommand = "JOG ";
    }
}
