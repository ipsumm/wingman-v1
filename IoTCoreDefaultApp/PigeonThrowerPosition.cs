﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IoTCoreDefaultApp
{
    public class PigeonThrowerPosition
    {
        #region Public Constructors

        public PigeonThrowerPosition(int id, string name, long pitch, long roll, long pull)
        {
            this.ID = id;
            this.Name = name;
            this.Pitch = pitch;
            this.Roll = roll;
            this.Pull = pull;
        }

        #endregion Public Constructors

        #region Public Properties

        public int ID { get; private set; }

        public string Name { get; private set; }

        public long Pitch { get; private set; }

        public long Pull { get; private set; }

        public long Roll { get; private set; }

        #endregion Public Properties

        #region Public Methods

        public sealed override string ToString()
        {
            return this.ID.ToString() + "," + this.Name + "," + this.Pitch.ToString() + "," + this.Roll.ToString() + "," + this.Pull.ToString();
        }

        #endregion Public Methods
    }
}
